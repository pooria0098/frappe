# -*- coding: utf-8 -*-
# Copyright (c) 2021, pooria and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document
# from apps.frappe import frappe
# from apps.frappe.frappe.model.document import Document


class LibraryMembership(Document):
    # check before submitting this document
    def before_submit(self):
        exists = frappe.db.exists(
            "Library Membership",
            {
                "library_member": self.library_member,
                # check for submitted documents
                "docstatus": 1,
                # check if the membership's end date is later than this membership's start date
                "to_date": (">", self.form_date),
            }
        )
        if exists:
            frappe.throw("There is an active membership for this member")

        loan_period = frappe.db.get_single_value("Library Settings", "loan_period")
        self.to_date = frappe.utils.add_days(self.form_date, loan_period)
